﻿using Moq;
using BL;
using DAL;
using Domain;

namespace Tests
{
    public class ManagerTests
    {
        private readonly Mock<IRepository> _mockRepository;
        private readonly Manager _manager;
        private readonly string _testFilePath;

        public ManagerTests()
        {
            _mockRepository = new Mock<IRepository>();
            _manager = new Manager(_mockRepository.Object);
            _testFilePath = "input.txt";
        }

        [Fact]
        public void FindWordCombinations_Returns_CorrectCombinations()
        {
            var wordsFromFile = new List<Word>
            {
                new Word { Name = "li" },
                new Word { Name = "st" },
                new Word { Name = "en" },
                new Word { Name = "l" },
                new Word { Name = "isten" },
                new Word { Name = "listen" },
                new Word { Name = "listen" },
                new Word { Name = "ddd" },
                new Word { Name = "ttt" },
                new Word { Name = "silent" },
            };

            _mockRepository.Setup(r => r.ReadAllWords(_testFilePath)).Returns(wordsFromFile);

            var expectedCombinations = new List<string>
            {
                "li + st + en = listen",
                "l + isten = listen",
            };

            var actualCombinations = _manager.FindWordCombinations(_testFilePath).ToList();

            foreach (var expectedCombination in expectedCombinations)
            {
                Assert.Contains(expectedCombination, actualCombinations);
            }
        }
    }
}

﻿using BL;

namespace UI_CA;

public class ConsoleUi
{
    private readonly IManager _mgr;

    public ConsoleUi(IManager mgr)
    {
        _mgr = mgr;
    }

    public void Run()
    {
        var resourcesDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../../DAL/", "resources");
        var filePath = Path.Combine(resourcesDir, "input.txt");

        foreach (var word in _mgr.FindWordCombinations(filePath))
        {
            Console.WriteLine(word);
        }
    }
}

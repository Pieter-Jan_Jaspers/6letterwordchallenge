﻿using BL;
using DAL;
using UI_CA;

IRepository repo = new Repository();
IManager mgr = new Manager(repo);

ConsoleUi consoleUi = new ConsoleUi(mgr);
consoleUi.Run();
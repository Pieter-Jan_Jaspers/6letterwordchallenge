﻿using Domain;

namespace DAL;

public interface IRepository
{
    IEnumerable<Word> ReadAllWords(string filePath);
}

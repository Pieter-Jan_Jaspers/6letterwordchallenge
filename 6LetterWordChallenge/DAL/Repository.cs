﻿using Domain;

namespace DAL
{
    public class Repository : IRepository
    {
        public IEnumerable<Word> ReadAllWords(string filePath)
        {
            ArgumentNullException.ThrowIfNull(filePath);

            var words = new List<Word>();

            try
            {
                using (var reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            words.Add(new Word { Name = line });
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return words;
        }
    }
}

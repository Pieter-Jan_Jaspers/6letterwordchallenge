﻿namespace BL;

public interface IManager
{
    IEnumerable<string> FindWordCombinations(string filePath);
}

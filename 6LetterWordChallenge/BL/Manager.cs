﻿using DAL;
using System.Collections.Generic;
using System.Linq;

namespace BL
{
    public class Manager : IManager
    {
        private readonly IRepository _repo;
        private readonly int MaxWordLength = 6;

        public Manager(IRepository repository)
        {
            _repo = repository;
        }

        public IEnumerable<string> FindWordCombinations(string filePath)
        {
            var words = _repo.ReadAllWords(filePath).Select(w => w.Name).Distinct().ToList();
            var wordsHashSet = new HashSet<string>(words);

            var sixLetterWords = words.Where(w => w.Length == MaxWordLength).ToList();
            var combinations = new List<string>();

            foreach (var target in sixLetterWords)
            {
                var currentCombination = new List<string>();
                FindCombinationsHelper(target, currentCombination, combinations, wordsHashSet);
            }

            return combinations;
        }

        private void FindCombinationsHelper(string target, List<string> currentCombination, List<string> combinations, HashSet<string> wordsHashSet)
        {
            if (target.Length == 0)
            {
                combinations.Add(string.Join(" + ", currentCombination) + " = " + string.Join("", currentCombination));
                return;
            }

            for (int i = 1; i <= target.Length; i++)
            {
                var prefix = target.Substring(0, i);
                var remaining = target.Substring(i);

                if (wordsHashSet.Contains(prefix))
                {
                    currentCombination.Add(prefix);
                    FindCombinationsHelper(remaining, currentCombination, combinations, wordsHashSet);
                    currentCombination.RemoveAt(currentCombination.Count - 1);
                }
            }
        }
    }
}
